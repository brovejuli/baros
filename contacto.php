<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgh overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Contact Section -->
    <section class="ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2>Contactanos</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 contact pb-60 pt-30">
                    <div class="row text-center">
                        <div class="col-sm-4 pb-xs-30"> <i class="ion-android-call icon-circle pos-s"></i> <a href="#" class="mt-15 i-block">0221 482-7771</a> </div>
                        <div class="col-sm-4 pb-xs-30"> <i class="ion-ios-location icon-circle pos-s"></i>
                            <p  class="mt-15">  21 # 198, La Plata,  <br />
                                Bs. As., Argentina </p>
                        </div>
                        <div class="col-sm-4 pb-xs-30"> <i class="ion-ios-email icon-circle pos-s"></i> <a href="mailto:medical@support.com"  class="mt-15 i-block">info@baros.com.ar</a> </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Map Section -->
        <div class="map">
            <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.5852203680984!2d-57.980998184846406!3d-34.91685718164062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95a2e7bf4d50747d%3A0xb2d6c26b69e95a1!2sBaros!5e0!3m2!1sen!2sar!4v1504287421849" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe></div>
        </div>
        <!-- Map Section -->
        <div class="container contact-form pt-50 mt-up">
            <div class="row">
                <div class="col-sm-12">
                    <h4>Escribinos</h4>
                    <!-- Contact FORM -->
                    <form class="contact-form mt-45" id="contact">

                        <!-- IF MAIL SENT SUCCESSFULLY -->
                        <div id="success">
                            <div role="alert" class="alert alert-success"> <strong>Thanks</strong> for using our template. Your message has been sent. </div>
                        </div>
                        <!-- END IF MAIL SENT SUCCESSFULLY -->

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-field">
                                    <input class="input-sm form-full" id="name" type="text" name="form-name" placeholder="Nombre">
                                </div>
                                <div class="form-field">
                                    <input class="input-sm form-full" id="email" type="text" name="form-email" placeholder="Email" >
                                </div>
                                <div class="form-field">
                                    <input class="input-sm form-full" id="sub" type="text" name="form-subject" placeholder="Asunto">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-field">
                                    <textarea class="form-full" id="message" rows="7" name="form-message" placeholder="Mensaje" ></textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 mt-30">
                                <button class="btn btn-md btn-color-line" type="button" id="submit" name="button">Enviar mensaje</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Contact FORM -->
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section -->



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->

<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
