<div class="container">

    <!--Footer Info -->
    <div class="row footer-info mb-60">
        <div class="col-md-5 col-sm-6 col-xs-12 mb-sm-30">
            <h5>CONTACTO</h5>
            <address>
                <i style="color:#bf0811" class="ion-ios-location fa-icons"></i> Calle 21 # 198 esq. 36 • La Plata • Buenos Aires • Argentina
            </address>
            <ul class="link-small">
                <li><a href="mailto:medical@support.com"><i style="color:#bf0811" class="ion-ios-email fa-icons"></i>info@baros.com.ar</a></li>
                <li><a><i style="color:#bf0811" class="ion-ios-telephone fa-icons"></i> 0221 482-7771</a></li>
            </ul>
            <div class="icons-hover-black"> <a href="javascript:avoid(0);"> <i class="fa fa-facebook"></i> </a> <a href="javascript:avoid(0);"> <i class="fa fa-twitter"></i> </a>  </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 mb-sm-30">
            <h5>NAVEGACIÓN</h5>
            <ul class="link blog-link">
                <li><a href="somos.php"><i class="fa fa-angle-double-right"></i> Quiénes somos</a></li>
                <li><a href="cirugia.php"><i class="fa fa-angle-double-right"></i> Cirugía de la obesidad</a></li>
                <li><a href="protos.php"><i class="fa fa-angle-double-right"></i> Tratamientos no quirúrgicos</a></li>
                <li><a href="noticias.php"><i class="fa fa-angle-double-right"></i> Noticias</a></li>
                <li><a href="galeria.php"><i class="fa fa-angle-double-right"></i> Galería de imágenes</a></li>
                <li><a href="apoyo_grupal.php"><i class="fa fa-angle-double-right"></i> Apoyo grupal</a></li>
                <li><a href="turnos.php"><i class="fa fa-angle-double-right"></i> Turnos online</a></li>
                <li><a href="contacto.php"><i class="fa fa-angle-double-right"></i> Contacto</a></li>
            </ul>

        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="newsletter">
                <h5><i style="color:#bf0811; font-size: 30px" class="fa fa-comments" aria-hidden="true"></i> CHAT ONLINE</h5>
                <form>
                    <input type="email" class="newsletter-input input-md newsletter-input mb-0" placeholder="Enter Your Email">
                    <button class="newsletter-btn btn btn-xs btn-color" type="submit" value=""><p>Enviar <i class="fa fa-angle-right mr-0"></i></p></button>
                </form>
            </div>
        </div>
    </div>
    <!-- End Footer Info -->
</div>

<!-- Copyright Bar -->
<div class="copyright">
    <div class="container">
        <p class=""> Baros © 2017 Todos los derechos reservados. </p>
    </div>
</div>
<!-- End Copyright Bar -->
