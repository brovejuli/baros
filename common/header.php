<header id="header" class="header header-1">

    <div class="nav-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="logo bounceInLeft"> <a href="index.php"><img src="assets/images/logo.png" alt="Baros"></a> </div>
                    <!-- Phone Menu button -->
                    <button id="menu" class="menu visible-xs"></button>
                </div>
                <div class="col-sm-9 nav-bg">
                    <nav class="navigation">
                        <ul>
                            <li><a href="somos.php">Quiénes somos</a> <i class="ion-ios-plus-empty visible-xs"></i>

                            </li>
                            <li><a href="javascript:avoid(0);">Qué hacemos</a> <i class="ion-ios-plus-empty visible-xs"></i>
                                <ul class="sub-nav">
                                    <li> <a href="cirugia.php">Cirugía de la obesidad</a> </li>
                                    <li> <a href="protos.php">Tratamientos no quirúrgicos</a> </li>

                                </ul>
                            </li>
                            <li><a href="noticias.php">Noticias</a> <i class="ion-ios-plus-empty visible-xs"></i>
                            </li>
                            <li><a href="galeria.php">Galería de imágenes</a> <i class="ion-ios-plus-empty visible-xs"></i>

                            <li><a href="apoyo_grupal.php">Apoyo grupal </a> <i class="ion-ios-plus-empty visible-xs"></i>

                            </li>
                            <li><a href="turnos.php">Turnos Online</a> <i class="ion-ios-plus-empty visible-xs"></i>

                            </li>
                            <li><a href="contacto.php">Contacto</a> <i class="ion-ios-plus-empty visible-xs"></i>

                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>