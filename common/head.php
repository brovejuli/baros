<meta charset="utf-8" />
<title>Cirugía de la obesidad</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<!-- Favicone Icon -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<!-- CSS -->
<link href="http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=lato:400,100,200,300,500" rel="stylesheet" type="text/css">
<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="assets/css/animate.css" rel="stylesheet" type="text/css">
<link href="assets/css/animate.min.css" rel="stylesheet" type="text/css">
<link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
<link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
<link href="assets/css/plugin/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/css/plugin/owl.transitions.css" rel="stylesheet" type="text/css">
<link href="assets/css/plugin/jquery.fancybox.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<!--Main Slider-->
<link href="assets/css/settings.css" type="text/css" rel="stylesheet" media="screen">
<link href="assets/css/layers.css" type="text/css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" type="text/css">
<link href="assets/css/responsive.css" rel="stylesheet" type="text/css">
<!--Theme Color-->
<link href="assets/css/theme-color/default.css" rel="stylesheet" id="theme-color" type="text/css">