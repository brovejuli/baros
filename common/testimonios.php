<h2 class="mt-sm">Testimonios</h2>
<div class="spacer-15"></div>
<div class=" text-center">
    <div class="post-media">
        <div id="testimonial" class=" testimonial nf-carousel-theme">
            <div class="item">
                <div class="quote bg-color light-color">
                    <p>“Con la ayuda de Baros, pude comprender mi situación y revertirla. Hoy luego de casi tres años vivo felizmente transitando un camino. ¡Gracias a Raúl y Walter por su excelente trabajo!, a Sandra y Sylvia por guiarme. A Mari y Marce por hacer de los trámites y esperas, amenos momentos y un agradecimiento muy especial a Vicky por su calidez, comprensión y contención permanente”.</p>
                </div>
                <div class="testimonial-pic"> <img class="item-container" src="assets/images/galeria/betiana.jpeg" alt="" /> </div>
            </div>
            <div class="item">
                <div class="quote bg-color light-color">
                    <p>“Desde enero de 2010 elegí este lugar para realizar el cambio. Hoy luego de pasar por muchas cosas, siempre contenida por el excelente grupo de profesionales, me siento feliz de haber dado tan importante paso... Tomo unas palabras de Raúl ´somos una familia que jamás podés abandonar´ y hoy los sigo eligiendo”. </p>
                </div>
                <div class="testimonial-pic"> <img class="item-container" src="assets/images/galeria/deleonkarina.jpeg" alt="" /> </div>
            </div>

        </div>
    </div>
</div>