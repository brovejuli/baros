<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgg overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <section class="section ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row mb-15">
                        <div class="col-md-12">
                            <h2>Apoyo grupal</h2>
                            <p class="lead"> Grupo de apoyo para pacientes y familiares</p>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="col-md-4 sinpadding">
                        <p>Mensualmente, tenes varias opciones para sentirte acompañado por el Grupo Interdisciplinario: Grupo para Pacientes Operados, Grupo de Apoyo para pacientes y Familiares y Grupos Educativos.</p>
                        <p>Los Grupos siempre son coordinados por Especialistas de Nuestro Staff, y en cada encuentro llevamos propuestas que hace que puedas ir construyendo herramientas para sostener tus logros de salud, descenso de peso y bienestar a largo plazo.</p>
                    </div>
                    <div class="col-md-4">
                        <p>Pueden participar pacientes operados y no operados, pacientes que recién ingresan al programa, amigos de pacientes y sus familiares. Está coordinado por todo el equipo interdisciplinario y en cada encuentro, además de conocernos con los nuevos pacientes, compartir la experiencia y dar testimonios, reservamos un espacio educativo en el que se abordan temas vinculados a la Cirugía Bariátrica de manera directa o indirecta, desarrollados por miembros del equipo y especialistas invitados.</p>
                    </div>
                    <div class="col-md-4">
                        <h5 style="color: "> Algunos de los temas que se abordan son: </h5>
                        <ul>
                            <li>Nutrición pre y post operatoria.</li>
                            <li> Aspectos psicológicos vinculados a la obesidad.</li>
                            <li> Obesidad y enfermedad endócrina, ginecológica y cardíaca.</li>
                            <li> Cirugía Plástica o Reparadora.</li>
                            <li>Estrés y alimentación.</li>
                            <li>Preparación para el cambio.</li>
                            <li>Cirugía Bariátrica: mitos y verdades.</li>
                            <li>Taller de ejercicio físico.</li>
                        </ul>
                    </div>
                </div>
                <div class="separador col-md-12">

                </div>

                <div class="col-md-12">
                    <div class="row mb-15">
                        <div class="col-md-12">
                            <h2>Actividades Recreativas</h2>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="col-md-4 frase sinpadding video">
                        <p>Durante el año organizamos actividades recreativas al aire libre, la Fiesta de la Primavera y hacia fin de año, realizamos la Despedida del año de Baros.</p>
                        <p>La Fiesta de la Primavera se organiza con fines exclusivamente recreativos con la ayuda de nuestros Acompañantes Bariátricos. Se desarrolla en el Prado Español  predio del Hospital Español, en Villa Elisa. Este espacio cuenta con siete hectáreas de bellísima arboleda añosa y mucho verde, es el sitio propicio para renovar nuestro compromiso con la salud y con la vida.</p>
                        <p>Es una actividad para toda la familia, donde compartimos juegos, concursos, kermeses, y una linda tarde entre amigos, celebrando que estamos juntos un año más y comprometiéndonos a continuar trabajando por la recuperación.</p>
                        <blockquote class="bg-color3"> Nuestro calendario cuenta con Grupos de Tratamiento, de Apoyo para los Operados, Clases de Gimnasia y Actividades Educativas y Recreativas para acompañarte a vos y a tu familia</blockquote>

                    </div>
                    <div class="col-md-8">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->


                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="assets/images/actividades/9.jpeg" alt="Chania">
                                    <div class="carousel-caption">
                                        <h3> Dia de la Primavera 2012 </h3>
                                    </div>
                                </div>

                                <div class="item">
                                    <img src="assets/images/actividades/10.jpeg" alt="Chicago">
                                    <div class="carousel-caption">
                                        <h3> Dia de la Primavera 2012 </h3>
                                    </div>
                                </div>

                                <div class="item">
                                    <img src="assets/images/actividades/11.jpeg" alt="New York">
                                    <div class="carousel-caption">
                                        <h3> Dia de la Primavera 2012 </h3>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="assets/images/actividades/12.jpeg" alt="New York">
                                    <div class="carousel-caption">
                                        <h3> Dia de la Primavera 2012 </h3>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="assets/images/actividades/13.jpeg" alt="New York">
                                    <div class="carousel-caption">
                                        <h3> Dia de la Primavera 2012 </h3>
                                    </div>
                                </div>
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>

        </div>
    </section>



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->



<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
