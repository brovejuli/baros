<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgb overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <section class="schedule pt-80 pb-60">
        <div class="container">
            <div class="row text-center">
                <div class="col-xs-12">
                    <h2>Profesionales</h2>
                </div>
            </div>
            <!-- work Filter -->
            <div class="row">
                <div class="specialist-tab mb-30 mt-15"> <span class="doctor-specialist"><span>Todos</span><i class="ion-ios-plus-empty"></i></span>
                    <ul class="filter-button-group ">
                        <li><a class="categories active" data-filter="*">Todos</a></li>
                        <li><a class="categories" data-filter=".cirugia_g">Cirugía general</a></li>
                        <li><a class="categories" data-filter=".cirugia_b">Cirugía baríatrica</a></li>
                        <li><a class="categories" data-filter=".psicologia">Psicología</a></li>
                        <li><a class="categories" data-filter=".nutricion">Nutrición</a></li>
                    </ul>
                </div>
            </div>
            <!-- End work Filter -->
            <!--Team Carousel -->
            <div class="row">
                <div id="isotope" class="isotope">
                    <div class="item cirugia_g">
                        <div class="team-item nf-col-padding">
                            <div class="team-item-img"> <img src="assets/images/doctor1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <h5>Dr. Raul Alejandro Cavo Frigerio</h5>
                                <p class="">Director Médico Baros</p>
                                <a href="solicitar_turno.php" class="btn btn-md btn-color-line  mt-15">Solicitar turno</a> </div>
                        </div>
                    </div>
                    <div class="item cirugia_g">
                        <div class="team-item nf-col-padding">
                            <div class="team-item-img"> <img src="assets/images/doctor1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <h5>Lic. Sylvia Gimeno</h5>
                                <p class="">Coordinadora Equipo </p>
                                <a href="solicitar_turno.php" class="btn btn-md btn-color-line  mt-15">Solicitar turno</a> </div>
                        </div>
                    </div>
                    <div class="item cirugia_b">
                        <div class="team-item nf-col-padding">
                            <div class="team-item-img"> <img src="assets/images/doctor1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <h5>Dr. Walter Coliandro</h5>
                                <p class="">Médico</p>
                                <a href="solicitar_turno.php" class="btn btn-md btn-color-line  mt-15">Solicitar turno</a> </div>
                        </div>
                    </div>
                    <div class="item psicologia">
                        <div class="team-item nf-col-padding">
                            <div class="team-item-img"> <img src="assets/images/doctor1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <h5>Dra.María Victoria Di Marco Entío</h5>
                                <p class="">Médica Nutrióloga</p>
                                <a href="solicitar_turno.php" class="btn btn-md btn-color-line  mt-15">Solicitar turno</a> </div>
                        </div>
                    </div>
                    <div class="item psicologia">
                        <div class="team-item nf-col-padding">
                            <div class="team-item-img"> <img src="assets/images/doctor1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <h5>Dr. Patricio Flaherty</h5>
                                <p class="">Especialista en Cardiología.</p>
                                <a href="solicitar_turno.php" class="btn btn-md btn-color-line  mt-15">Solicitar turno</a> </div>
                        </div>
                    </div>
                    <div class="item nutricion">
                        <div class="team-item nf-col-padding">
                            <div class="team-item-img"> <img src="assets/images/doctor1.jpg" alt="" />
                            </div>
                            <div class="team-item-info">
                                <h5>Dra. Verónica Contreras</h5>
                                <p class="">Médica</p>
                                <a href="solicitar_turno.php" class="btn btn-md btn-color-line  mt-15">Solicitar turno</a> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Team Carousel -->

        </div>
    </section>
    <!-- Section -->


    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->

<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
