<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imge overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Blog Section -->
    <section id="blog" class="wow fadeIn ptb-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 mb-xs-30 mb-sm-60">
                    <h2 class="mt-sm">Noticias </h2>
                    <div class="spacer-15"></div>
                    <div class="row">
                        <div class="col-md-4 mb-xs-30">
                            <div class="blog-post">
                                <div class="post-media"> <img src="assets/images/blog/balanza.jpg" alt=""> <span class="event-calender blog-date"> 11/01 </span> </div>
                                <div class="post-header">
                                    <h4> Re-ganancia de Peso después de la Cirugía: me puede pasar? Por qué?</h4>
                                </div>
                                <div class="post-entry">
                                    <p>Te preocupa recuperar el Peso despues de operad@? La cirugia bariatrica es una herramienta poderosísima pero no infalible y hay mucho por aprender para que no te pase. El lunes 11 de enero a las 19 hs en el Hospital Español vení a compartir un espacio educativo y de reflexión para poder prevenir, antes que curar. Tips, testimonios, experiencias en una charla-taller para vos y tu familia. Anotate, la entrada es libre y gratuita! 221-4827771</p>
                                </div>
                                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-xs-30">
                            <div class="blog-post">
                                <div class="post-media"> <img src="assets/images/02.jpg" alt="" /> <span class="event-calender blog-date"> 08/03 </span> </div>
                                <div class="post-header">
                                    <h4> Obesidad y vulnerabilidad psicológica</h4>
                                </div>
                                <div class="post-entry">
                                    <p>El INADi ha considerado a la obesidad la 2da causa de discriminación laboral en Argentina; el 78 % de las personas encuestadas por ese organismo reconocen haber discriminado a alguien por su sobrepeso u Obesidad. Los niños y adolescentes obesos padecen frecuentemente Bulling (hostigamiento). Decir además que un obeso no encuentra talle de ropa (en un país con Ley de Talles), que no encuentra butaca en el cine, en el avión o en el colectivo.</p>
                                </div>
                                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-xs-30">
                            <div class="blog-post">
                                <div class="post-media"> <img src="assets/images/02.jpg" alt="" /> <span class="event-calender blog-date"> 08/03 </span> </div>
                                <div class="post-header">
                                    <h4> Obesidad y vulnerabilidad psicológica</h4>
                                </div>
                                <div class="post-entry">
                                    <p>El INADi ha considerado a la obesidad la 2da causa de discriminación laboral en Argentina; el 78 % de las personas encuestadas por ese organismo reconocen haber discriminado a alguien por su sobrepeso u Obesidad. Los niños y adolescentes obesos padecen frecuentemente Bulling (hostigamiento). Decir además que un obeso no encuentra talle de ropa (en un país con Ley de Talles), que no encuentra butaca en el cine, en el avión o en el colectivo.</p>
                                </div>
                                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-xs-30">
                            <div class="blog-post">
                                <div class="post-media"> <img src="assets/images/blog/balanza.jpg" alt=""> <span class="event-calender blog-date"> 11/01 </span> </div>
                                <div class="post-header">
                                    <h4> Re-ganancia de Peso después de la Cirugía: me puede pasar? Por qué?</h4>
                                </div>
                                <div class="post-entry">
                                    <p>Te preocupa recuperar el Peso despues de operad@? La cirugia bariatrica es una herramienta poderosísima pero no infalible y hay mucho por aprender para que no te pase. El lunes 11 de enero a las 19 hs en el Hospital Español vení a compartir un espacio educativo y de reflexión para poder prevenir, antes que curar. Tips, testimonios, experiencias en una charla-taller para vos y tu familia. Anotate, la entrada es libre y gratuita! 221-4827771</p>
                                </div>
                                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-xs-30">
                            <div class="blog-post">
                                <div class="post-media"> <img src="assets/images/02.jpg" alt="" /> <span class="event-calender blog-date"> 08/03 </span> </div>
                                <div class="post-header">
                                    <h4> Obesidad y vulnerabilidad psicológica</h4>
                                </div>
                                <div class="post-entry">
                                    <p>El INADi ha considerado a la obesidad la 2da causa de discriminación laboral en Argentina; el 78 % de las personas encuestadas por ese organismo reconocen haber discriminado a alguien por su sobrepeso u Obesidad. Los niños y adolescentes obesos padecen frecuentemente Bulling (hostigamiento). Decir además que un obeso no encuentra talle de ropa (en un país con Ley de Talles), que no encuentra butaca en el cine, en el avión o en el colectivo.</p>
                                </div>
                                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-xs-30">
                            <div class="blog-post">
                                <div class="post-media"> <img src="assets/images/02.jpg" alt="" /> <span class="event-calender blog-date"> 08/03 </span> </div>
                                <div class="post-header">
                                    <h4> Obesidad y vulnerabilidad psicológica</h4>
                                </div>
                                <div class="post-entry">
                                    <p>El INADi ha considerado a la obesidad la 2da causa de discriminación laboral en Argentina; el 78 % de las personas encuestadas por ese organismo reconocen haber discriminado a alguien por su sobrepeso u Obesidad. Los niños y adolescentes obesos padecen frecuentemente Bulling (hostigamiento). Decir además que un obeso no encuentra talle de ropa (en un país con Ley de Talles), que no encuentra butaca en el cine, en el avión o en el colectivo.</p>
                                </div>
                                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Pagination Nav -->
                <div class="pagination-nav text-center mtb-60 mtb-xs-30 col-md-12">
                    <ul>
                        <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
                <!-- End Pagination Nav -->
            </div>
        </div>
    </section>
    <!-- End Blog Section -->



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->

<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
