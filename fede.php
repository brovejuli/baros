<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" type="image/x-icon" href="http://www.cidu.com.ar/favicon.ico">
    <title>Centro M&eacute;dico C.I.D.U.</title>
    <link rel="icon" type="image/png" href="http://www.cidu.com.ar/images/logo.png" />
    <link rel="stylesheet" href="http://www.cidu.com.ar/css/style.css" type="text/css">
    <link rel="stylesheet" href="http://www.cidu.com.ar/css/font-awesome/css/font-awesome.css">
    <script src="http://www.cidu.com.ar/js/jquery-1.11.1.min.js"></script>
    <script src="http://www.cidu.com.ar/js/jquery-migrate-1.2.1.min.js"></script>
    <!-- Slicknav !-->
    <script src="http://www.cidu.com.ar/js/slickNav/jquery.slicknav.min.js"></script>
    <script src="http://www.cidu.com.ar/js/slickNav.js"></script>
    <link rel="stylesheet" href="http://www.cidu.com.ar/js/slickNav/slicknav.css">
    <!-- Royal Slider !-->
    <script src="http://www.cidu.com.ar/js/royalSlider/jquery.royalslider.min.js"></script>
    <link rel="stylesheet" href="http://www.cidu.com.ar/js/royalSlider/royalslider.css">
    <link rel="stylesheet" href="http://www.cidu.com.ar/js/royalSlider/rs-default.css">
    <!-- Animate !-->
    <link rel="stylesheet" href="http://www.cidu.com.ar/js/animateit/animations.css">
    <link rel="stylesheet" href="http://www.cidu.com.ar/js/animateit/animations-ie-fix.css">
    <!-- Modal Windows !-->
    <script src="http://www.cidu.com.ar/js/modalWindows/modalEffects.js"></script>
    <link rel="stylesheet" href="http://www.cidu.com.ar/js/modalWindows/component.css">
    <!-- Validar !-->
    <script type="text/javascript" src="http://www.cidu.com.ar/js/jquery.validate.1.5.2.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    <!-- Autocompletar !-->
    <link rel="stylesheet" type="text/css" href="http://www.cidu.com.ar/js/autocomplete/jquery.autocomplete.css">
    <script type="text/javascript" src="http://www.cidu.com.ar/js/autocomplete/jquery.autocomplete.js"></script>
    <!-- Google Map !-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script type="text/javascript" src="http://www.cidu.com.ar/js/jquery.map.js"></script>
    <script src="http://www.cidu.com.ar/js/index.js"></script>
</head>

<body>
<div>
    <!--DIV PARA MMENU-->
    <div class="general main-wrap" id="general">
        <header class="">
            <div class="header">
                <!-- MOBILE MMENU -->
                <nav id="menu-mobile">
                    <ul>
                        <li><a href="http://www.cidu.com.ar/index.php#general">INICIO</a></li>
                        <li><a href="http://www.cidu.com.ar/index.php#turnos">TURNOS</a></li>
                        <li><a href="http://www.cidu.com.ar/index.php#info">C.I.D.U.</a></li>
                        <li><a href="http://www.cidu.com.ar/index.php#contacto">CONTACTO</a></li>
                    </ul>
                </nav>
                <nav id="menu" class="animatedParent animateOnce" data-sequence="100">
                    <ul>
                        <li id="1" class="bubble-float-bottom selected animated fadeInDown" data-id="1"><a href="http://www.cidu.com.ar/index.php#general">INICIO</a></li>
                        <li>
                            <spam>/</spam>
                        </li>
                        <li id="2" class=" animated fadeInDown" data-id="2"><a href="http://www.cidu.com.ar/index.php#turnos">TURNOS</a></li>
                        <li>
                            <spam>/</spam>
                        </li>
                        <li id="3" class=" animated fadeInDown" data-id="3"><a href="http://www.cidu.com.ar/index.php#info">C.I.D.U.</a></li>
                        <li>
                            <spam>/</spam>
                        </li>
                        <li id="4" class=" animated fadeInDown" data-id="4"><a href="http://www.cidu.com.ar/index.php#contacto">CONTACTO</a></li>
                    </ul>
                </nav>
                <div class="top-button hide">
                    <a href="#general"><img src="http://www.cidu.com.ar/images/up-arrow-grey.png"></a>
                </div>
                <div class="header-central">
                    <div id="logo" class="logo"> <img src="http://www.cidu.com.ar/images/logo.png" alt="Centro Medico CIDU" name="Loco-CIDU"> </div>
                    <div class="consultas">
                        <p class="small">Consultas al:</p>
                        <p class="big">+54 381 431-3244</p>
                        <p class="small">HORARIO DE ATENCIÓN LU A VI 9 A 12 Y 17 A 21 HS.</p>
                    </div>
                </div>
            </div>
        </header>
        <section class="info animatedParent animateOnce" data-sequence="150" id="info">
            <h2>DR ALEJANDRO BULACIO</h2>
            <div class="contenedor-info animatedParent animateOnce" data-sequence="150">
                <ul>
                    <li class="animated fadeInLeft" data-id="1">Titulo Medico otorgado por la Facultad de Medicina de Tucumán.</li>
                    <li class="animated fadeInLeft" data-id="2">Especializado en el Hospital de Agudos Juan A. Fernández de Bs As.</li>
                    <li class="animated fadeInLeft" data-id="3">Jefe de residentes en Hospital Fernández de Bs As.</li>
                    <li class="animated fadeInLeft" data-id="4">Especialidad en Urología en la facultad de Medicina de la Universidad de Buenos Aires (UBA).</li>
                    <li class="animated fadeInLeft" data-id="5">Fellow Del Hospital Italiano de Bs As en Cirugía Reconstructiva de Uretra.</li>
                    <li class="animated fadeInLeft" data-id="6">Miembro de Staff del Centro Integral de Urología,</li>
                    <li class="animated fadeInLeft" data-id="7">Encargado de la Sección de Cirugía Reconstructiva de Uretra.</li>
                </ul>
            </div>
            <div class="contenedor-imagenes animatedParent animateOnce">
                <div class="rs-imagenes-cidu rsDefault animated fadeInRight">
                    <!-- simple image slide --><img class="rsImg" src="http://www.cidu.com.ar/images/cidu.png" alt="" />
                    <!--<img class="rsImg" src="http://www.cidu.com.ar/images/cidu.png" alt="" /> <img class="rsImg" src="http://www.cidu.com.ar/images/cidu.png" alt="" /> <img class="rsImg" src="http://www.cidu.com.ar/images/cidu.png" alt="" /> <img class="rsImg" src="http://www.cidu.com.ar/images/cidu.png" alt="" />--></div>
            </div>
        </section><br/><br/>
        <section class="turnos animatedParent" id="turnos">
            <h2>SOLICITAR TURNO<span>Seleccionar el turno y se agendará al instante</span></h2>
            <div class="results animated flipInX">
                <div class="label"> <img src="http://www.cidu.com.ar/images/profesionales/thumb/bulacio.jpg">
                    <ul class="info">
                        <li><span class="noMobile">Profesional:</span> <strong> <a href="http://www.cidu.com.ar/profesionales.php?idprofesional=1">DR ALEJANDRO BULACIO</a></strong></li>
                        <li><span class="noMobile">Especialidad: </span><strong>UROLOGIA</strong></li>
                        <li>
                            <div class="noMobile">E-mail: <strong></strong></div>
                        </li>
                    </ul>
                </div>
                <div class="arrows"><a href="?avanzar=&idprofesional=1&fdate=06/10/2017&sdate=09/10/2017"><i class="fa fa-caret-left left"></i></a>
                    <a href="?avanzar=&idprofesional=1&fdate=12/10/2017&sdate=09/10/2017"><i class="fa fa-caret-right right"></i></a>
                </div>
                <ul class="days">
                    <li id="day">
                        <div class="title">LUNES<br>09/10/17</div>
                        <ul id="turno">
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=16:30&int=15">
                                <li id="check">16:30</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=16:45&int=15">
                                <li id="check">16:45</li>
                            </a>
                            <li id="uncheck">17:00</li>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=17:15&int=15">
                                <li id="check">17:15</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=17:30&int=15">
                                <li id="check">17:30</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=17:45&int=15">
                                <li id="check">17:45</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=18:00&int=15">
                                <li id="check">18:00</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=18:15&int=15">
                                <li id="check">18:15</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=18:30&int=15">
                                <li id="check">18:30</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=18:45&int=15">
                                <li id="check">18:45</li>
                            </a>
                        </ul>
                    </li>
                    <li id="day">
                        <div class="title">MARTES<br>10/10/17</div>
                        <ul id="turno">
                            <li id="uncheck">17:00</li>
                            <li id="uncheck">17:15</li>
                            <li id="uncheck">17:30</li>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=17:45&int=15">
                                <li id="check">17:45</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=18:00&int=15">
                                <li id="check">18:00</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=18:15&int=15">
                                <li id="check">18:15</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=18:30&int=15">
                                <li id="check">18:30</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=18:45&int=15">
                                <li id="check">18:45</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=19:00&int=15">
                                <li id="check">19:00</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=19:15&int=15">
                                <li id="check">19:15</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=19:30&int=15">
                                <li id="check">19:30</li>
                            </a>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-10&hora=19:45&int=15">
                                <li id="check">19:45</li>
                            </a>
                        </ul>
                    </li>
                    <li id="day">
                        <div class="title">MI&Eacute;RCOLES<br>11/10/17</div>
                        <ul id="turno"></ul>
                    </li>
                </ul>
            </div>
        </section>
        <section class="contacto animatedParent animateOnce" data-sequence="150" id="contacto">
            <h2 class="animated fadeInLeft" data-id="1">CONTACTO</h2>
            <form id="frmContacto" onsubmit="return false" class="animatedParent animateOnce" data-sequence="150">
                <h3>COMPLETAR FORMULARIO<span>obligatorio *</span></h3> <span class="required">*</span><input type="text" name="nombre" id="nombre" placeholder="Ingresa tu nombre"
                                                                                                              class="animated fadeInLeft" data-id="1"> <span class="required">*</span><input type="text" name="apellido" placeholder="Ingresa tu apellido"
                                                                                                                                                                                             class="animated fadeInLeft" data-id="2"> <span class="required">*</span><input type="mail" name="email" placeholder="Ingresa tu e-mail" class="animated fadeInLeft"
                                                                                                                                                                                                                                                                            data-id="3"> <input type="text" name="telefono" placeholder="Ingresa tu telefono" class="animated fadeInLeft" data-id="4"> <span class="required">*</span>
                <textarea
                    name="mensaje" placeholder="Ingresa tu mensaje" class="animated fadeInLeft" data-id="5"></textarea> <input type="submit" value="ENVIAR"> <input type="hidden" name="action" value="frmcontacto" /> </form>
            <div id="location">
                <div class="maps-wrapper animatedParent animateOnce">
                    <div id="maps-google">
                        <div id="map" class="map fleft animated fadeInRight"></div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="animatedParent animateOnce" data-sequence="150">
            <div class="main-wrap">
                <div class="social"> <i class="fa fa-twitter-square fa-4x animated bounceInUp" data-id="1"></i> <i class="fa fa-google-plus-square fa-4x animated bounceInUp" data-id="2"></i>            <i class="fa fa-facebook-square fa-4x animated bounceInUp" data-id="3"></i> </div> <span class="cidu">2012-2014 C.I.D.U.</span> <a target="_blank"
                                                                                                                                                                                                                                                                                                                                             href="http://www.powerbyte.com.ar"><span class="powerbyte">Powered: Powerbyte Soluciones Informáticas</span></a> </div>
        </footer>
    </div>
</div>
<script src="http://www.cidu.com.ar/js/animateit/css3-animate-it.js"></script>
</body>

</html>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-69948030-1', 'auto'); ga('send', 'pageview');
</script>


<section class="appointment-section pos-r pt-80">
    <div class="booking-section">
        <div class="container">
            <div id="success" class="appointment_succses">
                <div role="alert" class="alert alert-success"> <strong>Thanks</strong> for using our template. Your message has been sent. </div>
            </div>
            <div class="col-sm-6 col-sm-offset-3 text-center mb-30">
                <h2>AGENDE SU TURNO AHORA</h2>
                <p class="lead">Completar los datos del formulario.</p>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="left-block col-md-2">
                        <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.5852203680984!2d-57.980998184846406!3d-34.91685718164062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95a2e7bf4d50747d%3A0xb2d6c26b69e95a1!2sBaros!5e0!3m2!1sen!2sar!4v1504287421849" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe></div>
                    </div>
                    <div class="right-block col-md-10">
                        <div class="appointment">
                            <form id="frmTurno" onsubmit="return false">
                                <ul>
                                    <li>Profesional: <span>Dr. Patricio Flaherty</span></li>
                                    <li>Turno: <span>16:30</span></li>
                                    <li>Paciente:</li>
                                </ul>
                                <div class="form-field-wrapper">
                                    <label for="mobile">DNI</label>
                                    <input type="text" id="dni" placeholder="Numero de Documento (solamente números)" class="input-sm form-full">
                                </div>
                                <div class="form-field-wrapper">
                                    <label for="email">PACIENTE</label>
                                    <input type="text" id="paciente" placeholder="Nombre completo" class="input-sm form-full">
                                </div>
                                <div class="form-field-wrapper">
                                    <label for="email">EMAIL</label>
                                    <input type="text" id="email" placeholder="E-mail" class="input-sm form-full">
                                </div>
                                <div class="form-field-wrapper mt-40">
                                    <button name="submit" id="submit" type="submit" value="AGENDAR" class="btn btn-md btn-color-line input-sm form-full">AGENDAR</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>