<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>
<!--loader--> 
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgb overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->
  
  <section class="ptb-80">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="box-resume">
            <div class="center-top doctor-info"> <img src="assets/images/dr-img.jpg" alt="" class="img-responsive"> </div>
            <ul>
              <li><strong>Especialidad</strong> Especialista en Cardiología</li>
              <li><strong>Email</strong> <a href="mailto:email@domain.com" title="" class="a-email">email@domain.com</a></li>
            </ul>
            <div class="center-bottom"> <a href="appointment.html" class="btn btn-md btn-color-line ">Dr. Patricio Flaherty</a> </div>
          </div>
        </div>
        <div class="col-sm-7 col-sm-offset-1 pt-xs-60">
          <h2>Dr. Patricio Flaherty</h2>
          <p class="lead">Lorem ipsum dolor sit amet, libero turpis non cras ligula, id commodo, aenean est in volutpat amet sodales.</p>
          <div class="we-do">
            <div class="doctor-details results">
              <div>
                <h4><i class="fa fa-calendar calendar"></i> SOLICITAR TURNO</h4>
              </div>

                <ul class="days">
                    <li id="day">
                        <div class="title"><span>LUNES </span>09/10/17</div>
                        <ul id="turno">
                            <a href="agendar_turno.php">
                                <li id="check">16:30</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">16:45</li>
                            </a>
                            <li id="uncheck">17:00</li>
                            <a href="http://www.cidu.com.ar/turno.php?profesional=1&fecha=2017-10-09&hora=17:15&int=15">
                                <li id="check">17:15</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">17:30</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">17:45</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:00</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:15</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:30</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:45</li>
                            </a>
                        </ul>
                    </li>
                    <li id="day">
                        <div class="title"><span>MARTES</span> 10/10/17</div>
                        <ul id="turno">
                            <li id="uncheck">17:00</li>
                            <li id="uncheck">17:15</li>
                            <li id="uncheck">17:30</li>
                            <a href="agendar_turno.php">
                                <li id="check">17:45</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:00</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:15</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:30</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:45</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">19:00</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">19:15</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">19:30</li>
                            </a>

                        </ul>
                    </li>
                    <li id="day">
                        <div class="title"><span>MI&Eacute;RCOLES</span> 11/10/17</div>
                        <ul id="turno">
                            <li id="uncheck">17:00</li>
                            <li id="uncheck">17:15</li>
                            <li id="uncheck">17:30</li>
                            <a href="agendar_turno.php">
                                <li id="check">17:45</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:00</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:15</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:30</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">18:45</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">19:00</li>
                            </a>
                            <a href="agendar_turno.php">
                                <li id="check">19:15</li>
                            </a>

                        </ul>
                    </li>
                </ul>

                <div class="arrows text-right"><a href=""><i class="fa fa-caret-left left"></i></a>
                    <a href=""><i class="fa fa-caret-right right"></i></a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->
  
  <!-- Scroll Top --> 
  <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a> 
  <!-- End Scroll Top --> 
  
</div>
<!-- Site Wraper End --> 



<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script> 
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script> 
<script src="assets/js/jquery-ui.js" type="text/javascript"></script> 
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script> 
<!-- revolution Js --> 
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script> 
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script> 
<!-- revolution Js --> 
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script> 
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script> 
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
