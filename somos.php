<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imga overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Service Details Section -->
    <section class="section ptb">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="row mb-15">
                        <div class="col-sm-12">
                            <h2>Quiénes somos</h2>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <p>Baros es el Primer Equipo de Cirugia Bariatrica de la ciudad de la Plata; con casi 10 años de experiencia y cerca de 1000 cirugias bariátricas realizadas, es la Institución conb mayor experiencia de nuestra región. </p>
                    <p>Integrada por un equipo interdisciplinario, dedicado a brindar atención integral al paciente con obesidad que requiere de una cirugía para poder bajar de peso, asistimos a pacientes de la mayoria de las Obras Sociales provenientes de La Plata y zona de influencia, Pehuajo, Bahia Blanca,Trenque Lauquen, Olavarria, Azul, Tandil, Brandsen, General Belgrano, Saliquello, Lobos, Chascomus y muchisimas ciudades y pueblos de nuestra provincia, conociendo profundamente la singularidad del ciudadano bonaerense, y ajustando la propuesta a la realidad cotidiana de nuestros pacientes. Vivimos en la misma provincia, y conocemos las necesidades e idiosincrasia del pueblo bonaerense. </p>
                    <p>Nuestro equipo tiene plena dispónibilidad, y la planificación anual de las actividades permite previsión en la organización de los viajes de los pacientes que no son de La Plata y alrededores.</p>
                    <div class="row mt-30">
                        <div class="col-xs-12 col-md-7">
                            <div class="hightlight_event">
                                <h4>Nuestra institución</h4>
                                <p>Desde nuestro nacimiento nos dedicamos al abordaje integral de la Cirugía Bariátrica con un equipo altamente especializado y capacitado, que acompaña a cada paciente durante todo el tratamiento. </p>
                                <p>Elegimos el nombre Baros porque es un vocablo griego que significa peso. La Cirugía Bariátrica (de baros: peso y atros: medicina) y es la especialidad quirúrgica indicada para el descenso de peso.</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <div class="hightlight_rightblock">
                                <figure> <img src="assets/images/consultorios.jpg" alt=""> </figure>
                                <div class="hightlight_content hidden-xs">
                                    <figure> <img src="assets/images/baros.png" alt=""> </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h4>Staff</h4>
                    <div class="our-team">
                        <ul>
                            <li> <a href="#" class="pull-left">
                                    <figure> <img src="assets/images/team-1.jpg" alt=""> </figure>
                                </a>
                                <div class="media-body"> <strong> Dr. Raul Alejandro Cavo Frigerio </strong>
                                    <p>Director Médico Baros </p>
                                    <a href="#" class="btn btn-md btn-color-line xs-hidden ">Ver CV</a> </div>
                            </li>
                            <li> <a href="#" class="pull-left">
                                    <figure> <img src="assets/images/team-2.jpg" alt=""> </figure>
                                </a>
                                <div class="media-body"> <strong> Lic. Sylvia Gimeno</strong>
                                    <p>Coordinadora Equipo Interdisciplinario Baros y Protos </p>
                                    <a href="#" class="btn btn-md btn-color-line xs-hidden ">Ver CV</a> </div>
                            </li>
                            <li></li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Dr. Walter Coliandro </span> I Médico</li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Inst. Beatriz Olea </span> I Instrumentadora Quirúrgica</li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Dra.María Victoria Di Marco Entío </span> I Médica Nutrióloga</li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Dr. Patricio Flaherty </span> I Médico </li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Dra. Verónica Contreras </span> I Médica Especialista </li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Prof. Selene Gatti </span> I Profesora Universitaria en Educación Física </li>
                            <li><i class="fa fa-angle-double-right"></i> <span style="font-weight: bold"> Prof. Luis Vazquez</span> I Profesor Universitario de Educación Física </li>

                        </ul>



                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Service Details Section End-->



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->


<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
