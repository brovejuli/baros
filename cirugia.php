<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgc overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Service Details Section -->
    <section class="section ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="row mb-15">
                        <div class="col-sm-12">
                            <h2>Cirugía de la obesiddad </h2>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <p>La Cirugía Bariátrica es el método mas eficaz conocido hasta la fecha, para controlar la Obesidad Mórbida, y ha sido descrita como un verdadero "Milagro Medico" (Melodie Moordhead) . Pero para que su eficacia pueda ser tal, fundamentalmente será necesario que el paciente realice una evaluación integral, una preparación específica para la vida despues de la cirugía, y esencialmente.... que decida cambiar su vida para siempre continuando con el tratamiento interdisciplinario luego de la operación. </p>
                    <p>La cirugia actúa según varios mecanismos, consistentes en reducir el tamaño del estómago, provocar mala absorción de nutrientes, o ambos.  </p>
                    <p>En Baros realizamos el By Pass Gástrico y la Gastrectomía Tubular o Manga Gástrica. Estas cirugías se realizan por vía laparoscópica, es decir, con pequeñas incisiones e instrumental de alta complejidad. La evaluación integral del paciente determinará cuál es la cirugia mas conveniente para su caso particular.</p>

                    <div class="col-md-6 pt-30 sinpadding video">
                        <h5 style="color:#bf0811">BY PASS GÁSTRICO</h5>
                        <iframe width="100%" height="300" src="http://baros.com.ar//plugins/system/videobox/player.php?video=http://www.baros.com.ar/images/videos/bypass.mp4&autoplay=0&start=" frameborder="0" allowfullscreen oallowfullscreen msallowfullscreen webkitallowfullscreen mozallowfullscreen style="display: block; background: #ffffff;"></iframe>
                    </div>
                    <div class="col-md-6 pt-30 sinpadding video">
                        <h5 style="color:#bf0811">GASTRECTOMÍA TUBULAR O MANGA GÁSTRICA</h5>
                        <iframe width="100%" height="300" src="http://baros.com.ar//plugins/system/videobox/player.php?video=http://www.baros.com.ar/images/videos/sleeve.mp4&autoplay=0&start=" frameborder="0" allowfullscreen oallowfullscreen msallowfullscreen webkitallowfullscreen mozallowfullscreen style="display: block; background: #ffffff;"></iframe>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 mb-xs-30">
                    <?php include("common/testimonios.php"); ?>

                </div>

            </div>
        </div>
    </section>
    <!-- Service Details Section End-->



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->



<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
