<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgb overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Appointment Section -->
    <section class="ptb-80 appointment-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="box-resume datos2">
                        <div class="center-top doctor-info"> <img src="assets/images/dr-img.jpg" alt="" class="img-responsive"> </div>
                        <ul>
                            <li>Turno: <span>04/07/2017</span></li>
                            <li>Hora: <span>16:30</span></li>
                        </ul>
                        <div class="center-bottom"> <a class="btn btn-md btn-color-line">Dr. Patricio Flaherty</a> </div>
                    </div>
                </div>
                <div class="col-sm-4 pt-xs-60">
                    <form id="frmTurno" onsubmit="return false">

                        <div class="form-field-wrapper col-md-12">
                            <label for="mobile">DNI</label>
                            <input type="text" id="dni" placeholder="Numero de Documento (números)" class="input-sm form-full">
                        </div>
                        <div class="form-field-wrapper col-md-12">
                            <label for="email">PACIENTE</label>
                            <input type="text" id="paciente" placeholder="Nombre completo" class="input-sm form-full">
                        </div>
                        <div class="form-field-wrapper col-md-12">
                            <label for="email">TELÉFONO</label>
                            <input type="text" id="telefono" placeholder="Teléfono" class="input-sm form-full">
                        </div>
                        <div class="form-field-wrapper col-md-12">
                            <label for="email">EMAIL</label>
                            <input type="text" id="email" placeholder="E-mail" class="input-sm form-full">
                        </div>
                        <div class="form-field-wrapper mt-30 col-md-12">
                            <button name="submit" id="submit" type="submit" value="AGENDAR" class="btn btn-md btn-color-line input-sm form-full">AGENDAR</button>
                        </div>

                    </form>
                        </div>
                <div class="col-md-4 datos">
                    <div><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.5850450411917!2d-57.98099818496008!3d-34.91686158037953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95a2e7bf4d50747d%3A0xb2d6c26b69e95a1!2sBaros!5e0!3m2!1sen!2sar!4v1507296812495" width="100%" height="300px" frameborder="0" style="border:0"></iframe></div>
                   <i class="ion-android-call"></i> &nbsp;<a href="#" class="mt-15 i-block">0221 482-7771</a> <br>
                   <i class="ion-ios-location"></i>&nbsp; <a>  21 # 198, La Plata, Bs. As., Argentina </a> <br>
                   <i class="ion-ios-email"></i> &nbsp;<a href="mailto:medical@support.com">info@baros.com.ar</a>
                </div>

                    </div>

        </div>
    </section>

    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->



<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
