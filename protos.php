<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgd overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Service Details Section -->
    <section class="section ptb">
        <div class="container">
            <div class="row">
                <div class="col-md-8 frase">
                    <div class="row mb-15">
                        <div class="col-md-12">
                            <h2 style="color: #1babb7;">Cirugía de la obesiddad </h2>
                        </div>
                    </div>
                    <p>La constante demanda de nuestros pacientes y sus familiares nos animó a enfrentar un nuevo desafío y desde abril de 2012, y como parte de las actividades de asistencia y prevención, nació Protos, el Programa de Tratamiento en Sobrepeso y Obesidad de Baros.</p>
                    <p>Protos es una palabra griega que significa lo que está primero. Porque creemos que lo primero es la prevención y detección precoz de factores de riesgo. Por eso, aquellas personas que padezcan algún grado de sobrepeso u obesidad y no necesiten una cirugía o que sí la requieran y además, desean intentar un tratamiento interdisciplinario de enfoque no quirúrgico, cuentan con un equipo especializado para su abordaje. </p>
                    <div class="divider"></div>
                    <br>
                    <span class="sub-title">Soporte grupal</span>
                    <h2 style="color: #1babb7;">Espacio de Encuentro, Educacion y Contencion</h2>
                    <p>Protos es un vocablo Griego que significa "Lo que está primero", y el nombre del Programa de Tratamiento para pacientes que no re1quieren Cirugía. Y es que "lo que esta primero" es el tratamiento interdisciplinario integral, la cirugía es una medida extrema. En Protos nos ocupamos de  acompañar a los pacientes mayores de 15 años a modificar paulatinamente sus hábitos para lograr un peso saludable y un vinculo mas confortable con la comida y el ejercicio. Contamos con un Espacio mensual de Encuentro, Educación y Reflexion para el intercambio constructivo de experiencias, el 4to lunes de cada mes a las 18 hs. en el Hospital Español.</p>
                    <p>Los pacientes que ingresan a Protos se incorporan por un sistema de Modulos Mensuales, que incluyen consultas con psicologas, médicas especialistas en nutricion, profesores de educación física, Espacio mensual de Encuentro, Educación y Contencion, Actividades Recreativas, Actualizaciones desde nuestra Face page Protos con consejos, tips y recetas saludables.</p>
                    <p>Escuchando sin juzgar, y acompañando para cambiar, es nuestro lema promoviendo cambios pequeños y graduales, que puedan sostenerse a largo plazo.</p>
                    <p>Cientos de Pacientes que logran franca mejoria y bienestar, avalan nuestro trabajo.</p>
                </div>
                <div class="col-md-4 col-xs-12 mb-xs-30 frase">
                    <img src="assets/images/logo_protos.jpg"><br>
                    <br>
                    <blockquote class="bg-color2"> Creemos que la prevención es el primer paso para lograr el cambio</blockquote>

<br>
                    <?php include("common/testimonios_protos.php"); ?>

                </div>

            </div>
        </div>
    </section>
    <!-- Service Details Section End-->



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->


<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
