<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imgf overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Gallery Section -->
    <div class="gallery ptb ">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 mb-15">
                    <h2>Galería </h2>
                    <p class="lead"> El antes y después de nuestros pacientes </p>
                </div>
            </div>

            <!--Gallery -->
            <div class="row">
                <div id="gallery" class="isotope galler col-md-8">
                    <div class="col-md-6 video sinpadding mb-30">
                        <div class="gallery-item">
                            <div class="gallery-item-img"> <img src="assets/images/galeria/betiana.jpeg" alt="" />
                                <div class="gallery-item-detail"> <a href="assets/images/galeria/betiana.jpeg" class="team-item-detail-inner light-color fancylight" data-fancybox-group="light"> <span class="icon-box fa fa-picture-o"></span> </a> </div>
                            </div>
                            <div class="gallery-item-info text-left">
                                <h5> Betiana Bruschini  </h5>
                                <p> 12 meses después de su operación</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 video sinpadding mb-30">
                        <div class="gallery-item">
                            <div class="gallery-item-img"> <img src="assets/images/galeria/butronjorge.jpeg" alt="" />
                                <div class="gallery-item-detail"> <a href="assets/images/galeria/butronjorge.jpeg" class="team-item-detail-inner light-color fancylight" data-fancybox-group="light"> <span class="icon-box fa fa-picture-o"></span> </a> </div>
                            </div>
                            <div class="gallery-item-info text-left">
                                <h5>Jorge Butron </h5>
                                <p>xxxx</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 video sinpadding mb-30">
                        <div class="gallery-item">
                            <div class="gallery-item-img"> <img src="assets/images/galeria/carlamazuchi.jpeg" alt="" />
                                <div class="gallery-item-detail"> <a href="assets/images/galeria/carlamazuchi.jpeg" class="team-item-detail-inner light-color fancylight" data-fancybox-group="light"> <span class="icon-box fa fa-picture-o"></span> </a> </div>
                            </div>
                            <div class="gallery-item-info text-left">
                                <h5>Carla Mazzuchi </h5>
                                <p>xxxx</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 video sinpadding mb-30">
                        <div class="gallery-item">
                            <div class="gallery-item-img"> <img src="assets/images/galeria/deleonkarina.jpeg" alt="" />
                                <div class="gallery-item-detail"> <a href="assets/images/galeria/deleonkarina.jpeg" class="team-item-detail-inner light-color fancylight" data-fancybox-group="light"> <span class="icon-box fa fa-picture-o"></span> </a> </div>
                            </div>
                            <div class="gallery-item-info text-left">
                                <h5>Karina de León</h5>
                                <p>xxxx</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 video sinpadding mb-30">
                        <div class="gallery-item">
                            <div class="gallery-item-img"> <img src="assets/images/galeria/hilda.jpeg" alt="" />
                                <div class="gallery-item-detail"> <a href="assets/images/galeria/hilda.jpeg" class="team-item-detail-inner light-color fancylight" data-fancybox-group="light"> <span class="icon-box fa fa-picture-o"></span> </a> </div>
                            </div>
                            <div class="gallery-item-info text-left">
                                <h5> Hilda Acosta </h5>
                                <p>xxxx</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 video sinpadding mb-30">
                        <div class="gallery-item">
                            <div class="gallery-item-img"> <img src="assets/images/galeria/lilianachielli.jpeg" alt="" />
                                <div class="gallery-item-detail"> <a href="assets/images/galeria/lilianachielli.jpeg" class="team-item-detail-inner light-color fancylight" data-fancybox-group="light"> <span class="icon-box fa fa-picture-o"></span> </a> </div>
                            </div>
                            <div class="gallery-item-info text-left">
                                <h5>Liliana Chielli </h5>
                                <p>xxxx</p>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination Nav -->
                    <div class="pagination-nav text-center mtb-60 mtb-xs-30 col-md-12">
                        <ul>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                    <!-- End Pagination Nav -->
                </div>


                <div class="col-md-4 col-xs-12">
                    <?php include("common/testimonios_sinfoto.php"); ?>

                </div>

            </div>
            <!--End Gallery-->

        </div>
    </div>



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->


<script src="assets/js/plugin/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script>
<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.fancybox.pack.js" type="text/javascript"></script>

<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
