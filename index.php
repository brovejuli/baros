﻿<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>
</head>
<body>
<!--loader-->
<div id="preloader">
  <div class="sk-circle">
    <div class="sk-circle1 sk-child"></div>
    <div class="sk-circle2 sk-child"></div>
    <div class="sk-circle3 sk-child"></div>
    <div class="sk-circle4 sk-child"></div>
    <div class="sk-circle5 sk-child"></div>
    <div class="sk-circle6 sk-child"></div>
    <div class="sk-circle7 sk-child"></div>
    <div class="sk-circle8 sk-child"></div>
    <div class="sk-circle9 sk-child"></div>
    <div class="sk-circle10 sk-child"></div>
    <div class="sk-circle11 sk-child"></div>
    <div class="sk-circle12 sk-child"></div>
  </div>
</div>
<!--loader--> 
<!-- Site Wraper -->
<div class="wrapper"> 
  
  <!-- HEADER -->
    <?php include("common/header.php"); ?>

    <!-- END HEADER -->
  <!--  Main Banner Start Here-->
  <div class="main-banner">
    <div id="rev_slider_34_1_wrapper" class="rev_slider_wrapper" data-alias="news-gallery34"> 
      <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
      <div id="rev_slider_34_1" class="rev_slider fullwidthabanner" data-version="5.0.7">
        <ul>
          <!-- SLIDE  -->
          <li data-index="rs-129">
            <!-- MAIN IMAGE --> 
            <img src="assets/images/banner/img3.jpg"  alt=""  class="rev-slidebg" >
            <!-- LAYERS --> 
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption Newspaper-Title tp-resizeme " 
									 id="slide-129-layer-1" 
									 data-x="['left','left','left','left']" data-hoffset="['100','50','50','30']" 
									 data-y="['top','top','top','center']" data-voffset="['165','135','105','0']" 
												data-fontsize="['50','50','50','30']"
									data-lineheight="['55','55','55','35']"
									data-width="['600','600','600','420']"
									data-height="none"
									data-whitespace="normal"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
									 data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									 data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" >
              <div class="banner-text">
                <h2>Brindamos atención <br> integral al paciente </h2>
                <p> Nuestro equipo tiene plena dispónibilidad, y la planificación anual de las actividades permite previsión en la organización de los viajes de los pacientes que no son de La Plata y alrededores.</p>
               </div>
            </div>
          </li>
          <!-- SLIDE  -->
          <li data-index="rs-130"   data-title="" data-description=""> 
            <!-- MAIN IMAGE --> 
            <img src="assets/images/banner/img2.jpg"  alt=""   class="rev-slidebg">
            <!-- LAYERS --> 
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption Newspaper-Title   tp-resizeme " 
									 id="slide-130-layer-1" 
									 data-x="['left','left','left','left']" data-hoffset="['100','50','50','30']" 
									 data-y="['top','top','top','center']" data-voffset="['165','135','105','0']" 
												data-fontsize="['50','50','50','30']"
									data-lineheight="['55','55','55','35']"
									data-width="['600','600','600','420']"
									data-height="none"
									data-whitespace="normal"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
									 data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									 data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on">
              <div class="banner-text">
                <h2>Somos un Equipo Interdisciplinario con amplia experiencia</h2>
                <p>  La planificación y el trabajo conjunto entre los diferentes profesionales que se capacitan constantemente para continuar brindando una atención personalizada y de excelencia.</p>
               </div>
            </div>
          </li>
          <!-- SLIDE  -->
          <li data-index="rs-131"  > 
            <!-- MAIN IMAGE --> 
            <img src="assets/images/banner/img4.jpg"  alt=""   class="rev-slidebg " >
            <!-- LAYERS --> 
            
            <!-- LAYER NR. 2 -->
            <div class="tp-caption Newspaper-Title   tp-resizeme " 
									 id="slide-131-layer-1" 
									 data-x="['left','left','left','left']" data-hoffset="['100','50','50','30']" 
									 data-y="['top','top','top','center']" data-voffset="['165','135','105','0']" 
												data-fontsize="['50','50','50','30']"
									data-lineheight="['55','55','55','35']"
									data-width="['600','600','600','420']"
									data-height="none"
									data-whitespace="normal"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
									 data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
									 data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
									data-start="1000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on">
              <div class="banner-text">
                <h2>Abordamos el problema desde la prevención y el tratamiento hasta la cirugía.</h2>
                <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever.</p>
               </div>
            </div>
          </li>
          <!-- SLIDE  -->
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
      </div>
    </div>
    <div class="appointment">
    	<div id="success" class="appointment_succses">
              <div role="alert" class="alert alert-success"> <strong>Thanks</strong> for using our template. Your message has been sent. </div>
            </div>
      <div class="container">

            <div class="col-md-2">
                <img src="assets/images/imc.png">
            </div>
          <div class="peso col-md-3">
            <input type="text" placeholder="Tu peso" id="name" class="input-sm form-full">
          </div>

          <div class="peso col-md-3">
            <input type="text" placeholder="Tu altura" id="mobile" class="input-sm form-full">
          </div>

          <div class="peso col-md-2">
            <button name="submit" id="submit" type="button" class="btn border-line btn-white-line input-sm form-full">Calcular</button>
          </div>
            <div class="peso col-md-2 tabla">
              <a href=""> <i class="fa fa-table"></i>  Ver tabla</a>
            </div>

      </div>
    </div>
  </div>
  <!--  Main Banner End Here-->





  
  <hr>

  <!-- Blog Section -->
  <section id="blog" class="wow fadeIn ptb-80">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12 mb-xs-30 mb-sm-60">
          <h2 class="mt-sm">Noticias </h2>
          <div class="spacer-15"></div>
          <div class="row">
            <div class="col-sm-6 mb-xs-30">
              <div class="blog-post">
                  <div class="post-media"> <img src="assets/images/blog/balanza.jpg" alt=""> <span class="event-calender blog-date"> 11/01 </span> </div>
                <div class="post-header">
                  <h4> Re-ganancia de Peso después de la Cirugía: me puede pasar? Por qué?</h4>
                </div>
                <div class="post-entry">
                  <p>Te preocupa recuperar el Peso despues de operad@? La cirugia bariatrica es una herramienta poderosísima pero no infalible y hay mucho por aprender para que no te pase. El lunes 11 de enero a las 19 hs en el Hospital Español vení a compartir un espacio educativo y de reflexión para poder prevenir, antes que curar. Tips, testimonios, experiencias en una charla-taller para vos y tu familia. Anotate, la entrada es libre y gratuita! 221-4827771</p>
                </div>
                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
              </div>
            </div>
            <div class="col-sm-6 mb-xs-30">
              <div class="blog-post">
                <div class="post-media"> <img src="assets/images/02.jpg" alt="" /> <span class="event-calender blog-date"> 08/03 </span> </div>
                <div class="post-header">
                  <h4> Obesidad y vulnerabilidad psicológica</h4>
                </div>
                <div class="post-entry">
                  <p>El INADi ha considerado a la obesidad la 2da causa de discriminación laboral en Argentina; el 78 % de las personas encuestadas por ese organismo reconocen haber discriminado a alguien por su sobrepeso u Obesidad. Los niños y adolescentes obesos padecen frecuentemente Bulling (hostigamiento). Decir además que un obeso no encuentra talle de ropa (en un país con Ley de Talles), que no encuentra butaca en el cine, en el avión o en el colectivo.</p>
                </div>
                <div class="post-more-link pull-left"><a href="nota.php" class="btn btn-md btn-color-line ">Leer más</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-xs-12 mb-xs-30">
            <?php include("common/testimonios.php"); ?>

        </div>
      </div>
    </div>
  </section>
  <!-- End Blog Section --> 
  <!-- FOOTER -->
  <footer class="footer pt-80">
      <?php include("common/footer.php"); ?>

  </footer>
  <!-- END FOOTER --> 
  
  <!-- Scroll Top --> 
  <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a> 
  <!-- End Scroll Top --> 
  
</div>
<!-- Site Wraper End --> 



<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>  
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script> 
<script src="assets/js/jquery-ui.js" type="text/javascript"></script> 
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script> 
<!-- revolution Js --> 
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script> 
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script> 
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script> 
<!-- revolution Js --> 
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script> 
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/appointment-index-page.js" type="text/javascript"></script>  
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
