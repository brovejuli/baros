<!DOCTYPE html>
<html>
<head>
    <?php include("common/head.php"); ?>

</head>
<body>
<!--loader-->
<div id="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</div>
<!--loader-->
<!-- Site Wraper -->
<div class="wrapper">

    <?php include("common/header.php"); ?>

    <!-- Intro Section -->
    <section class="inner-intro bg-imga overlay-bg-color light-color parallax parallax-background">
        <div class="container">

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- End Intro Section -->

    <!-- Blog Post Section -->
    <section class="ptb ptb-sm-80">
        <div class="container">
            <div class="row">
                <!-- Post Bar -->
                <div class="col-md-8 blog-post-hr post-section">
                    <div class="blog-post mb-30">
                        <div class="post-meta">
                            <div class="post-more-link pull-right">
                                <div class="icons-hover-black"> <a href="#" class="facebook-icon"> <i class="fa fa-facebook"></i> </a> <a href="#" class="twitter-icon"> <i class="fa fa-twitter"></i> </a> </div>
                                <a class="btn btn-md btn-color-line "> <i class="ion-android-share-alt"></i></a> </div>
                        </div>
                        <div class="post-header">
                            <h2>Re-ganancia de Peso después de la Cirugía: <br> me puede pasar? Por qué?</h2>
                        </div>
                        <div class="post-media"> <img src="assets/images/blog/balanza.jpg" alt=""> <span class="event-calender blog-date"> 11/01 </span> </div>
                        <div class="post-entry">
                            <p>Te preocupa recuperar el Peso despues de operad@? La cirugia bariatrica es una herramienta poderosísima pero no infalible y hay mucho por aprender para que no te pase. El lunes 11 de enero a las 19 hs en el Hospital Español vení a compartir un espacio educativo y de reflexión para poder prevenir, antes que curar. Tips, testimonios, experiencias en una charla-taller para vos y tu familia. Anotate, la entrada es libre y gratuita! 221-4827771</p>
                        </div>
                    </div>

                    <div class="clearfix"></div>


                </div>
                <!-- End Post Bar -->
<div class="col-md-4">
    <?php include("common/testimonios.php"); ?>

</div>

            </div>
        </div>
    </section>
    <!-- End Blog Post Section -->



    <!-- FOOTER -->
    <footer class="footer pt-80">
        <?php include("common/footer.php"); ?>

    </footer>
    <!-- END FOOTER -->

    <!-- Scroll Top -->
    <a class="scroll-top"> <i class="fa fa-angle-double-up"></i> </a>
    <!-- End Scroll Top -->

</div>
<!-- Site Wraper End -->


<script src="assets/js/jquery-1.12.4.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/jquery.easing.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/owl.carousel.min.js" type="text/javascript"></script>
<!-- revolution Js -->
<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
<!-- revolution Js -->
<script src="assets/js/plugin/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/plugin/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
